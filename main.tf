provider "aws" {
    region = "ap-south-1"
}

variable "cidr_blocks"{
  description = "vpc and subnet cidr blocks"
  type = list(string)
}

variable "environment"{
  description ="devolpment environment"
}
resource "aws_vpc" "devolpment-vpc" {
    cidr_block = var.cidr_blocks[0]
    tags = {
    Name = var.environment
  }
}

resource "aws_subnet" "dev-public-subnet-1a"{
    vpc_id = aws_vpc.devolpment-vpc.id
    cidr_block = var.cidr_blocks[1]
    availability_zone = "ap-south-1a"
    tags = {
    Name = "dev-public-subnet-1a"
  }
}

data "aws_vpc" "existing_vpc" {
    tags = {
    Name = "dev"
  }
}

resource "aws_subnet" "dev-public-subnet-1b"{
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = var.cidr_blocks[2]
    availability_zone = "ap-south-1b"
    tags = {
    Name = "dev-public-subnet-1b"
  }
}

resource "aws_subnet" "dev-public-subnet-1c"{
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = var.cidr_blocks[3]
    availability_zone = "ap-south-1c"
    tags = {
    Name = "dev-public-subnet-1c"
  }
}

output "aws_dev_vpc_id" {
    value = aws_vpc.devolpment-vpc.id
}

output "aws_subnet_1_id" {
    value = aws_subnet.dev-public-subnet-1a.id
}

output "aws_subnet_2_id" {
    value = aws_subnet.dev-public-subnet-1b.id
}

output "aws_subnet_3_id" {
    value = aws_subnet.dev-public-subnet-1c.id
}
